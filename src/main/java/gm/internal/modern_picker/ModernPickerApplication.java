package gm.internal.modern_picker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ModernPickerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ModernPickerApplication.class, args);
	}

}
